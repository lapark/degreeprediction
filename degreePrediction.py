#!/usr/bin/python

import time
import cv2
import random

# trained weights for face detection
# wget https://raw.githubusercontent.com/opencv/opencv/3.4/data/haarcascades/haarcascade_frontalface_default.xml
cascPath = "./haarcascade_frontalface_default.xml"


# degrees to choose from
degreeSet = ["Data Science", "Mathematics", "Computing", "Science", "Engineering", "Business", "Teaching", "Nursing", "Arts", "Music"]
# set weights to bias towards data science
degreeWeights = [len(degreeSet) - a for a in range(len(degreeSet))]


print("HDFA: Higher Degree Facial Analysis v0.4\nCopyright Phrenology Inc. 2018")


def main():

    # setup camera and windows
    camera_port = 0
    camera = cv2.VideoCapture(camera_port)
    time.sleep(0.1)  # If you don't wait, the image will be dark
    cv2.startWindowThread()
    cv2.namedWindow("Camera")

    # Create the haar cascade face detector
    faceCascade = cv2.CascadeClassifier(cascPath)

    # increasing bias in weights
    loopCounter = 0

    ## loop until esc is pressed
    while (cv2.waitKey(100) != 27):

        
        return_value, image = camera.read()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags = cv2.CASCADE_SCALE_IMAGE
        )

        print("Found {0} faces!".format(len(faces)))

        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
            currentDegreeWeights = [a ** (loopCounter/10) for a in degreeWeights]
            degreeText = random.choices(degreeSet, weights = currentDegreeWeights, k = 1)
            cv2.putText(image, degreeText[0], (x, y-10), cv2.FONT_HERSHEY_DUPLEX, 1.0,
                        (0, 255, 0), lineType=cv2.LINE_AA)
            
        cv2.imshow("Faces found", image)

        loopCounter += 1
        
    del(camera)  # so that others can use the camera as soon as possible

    cv2.destroyAllWindows()


main()





